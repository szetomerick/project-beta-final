# CarCar
Broad System Design is attached below.

There should be one bounded context with the aggregate root as inventory and Sales as the aggregate. Due to the system design, In order to make an accurate sales record with a identifier (VIN), which is polled from the inventory model, the sales model is completely dependent on the inventory model. Inventory can continue to exist as a standalone app. Sales Model ceases to exist without the dependent of the Inventory model.
![](images/EXCALI1.png)
Models that I have created for the sales include SalesRecord, Salesperson, customer, and automobile VO. Salesrecord has three foreign keys to, salesperson, customer, and AutomobileVo. AutomobileVo will get data from inventory model through a poller.py In order to create a vin in the inventory model, a manufacterer, then a vehicle model, then an automobile has to be created first.
![](images/EXCALI2.png)

# Sales Links and Inputs for insomnia

Post/Get Sales Records: http://localhost:8090/api/salesrecords/

JsonBody to Post

In order to successfully post a record, sales_person, Customer, and carvin should be populated with id fields. This is because sales_person, customer, and carvin are all foreign objects to salesperson, customer, and AutomobileVO respectively. Price is a standalone attribute field.

```python
{
			"sales_person": "1",
			"customer": "1",
			"carvin": "2",
			"price": "20000"

		}
```

[http://localhost:8090/api/automobilevo/](http://localhost:8090/api/automobilevo/)

Should come out in this format after sending a pull request.

Get Automobile VO( data to be populated with Inventory VIN information when creating an automobile through inventory. By utilizing a poller, IE poller.py. The inventory API  sends Car data to the sales microservice.

```python
{
	"automobilevo": [
		{
			"vin": "4Y1SL65848Z411439",
			"id": 1
		},
	]
}
```

Create a SalesPerson [http://localhost:8090/api/salespersons/](http://localhost:8090/api/salespersons/)

JSON body in order to post

The salesperson needs name and a number field in order to post a salesperson instance.

```python
{
			"name": "Gaby The best SalesMan",
			"number": "6263171717"
		}
```

Create a Customer [http://localhost:8090/api/customers/](http://localhost:8090/api/customers/)

Json Body in order to post

The customer needs a name, address, and number field to properly post a customer instance into the customer model.







----------------------------------------------------------------------------------------------
Car Models, Car Manufacturer's and Automobile's have been provided.