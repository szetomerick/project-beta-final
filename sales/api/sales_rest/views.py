from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json
from .models import Salesperson, Customer, AutomobileVO, Salesrecord

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin"
    ]

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "name",
        "number",
        "id"
    ]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "number",
        "id"
    ]

class SalesRecordEncoder(ModelEncoder):
    model = Salesrecord
    properties = [
        "carvin",
        "sales_person",
        "customer",
        "price",
        "id",
    ]
    #encoders are used only for rorein keyus
    encoders = {
        "carvin": AutomobileVOEncoder,
        "sales_person" : SalespersonListEncoder,
        "customer" : CustomerListEncoder,

    }




# Create your views here.
class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "name",
        "number",
        "id"
    ]



class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "id"
    ]


class SalesRecordEncoder(ModelEncoder):
    model = Salesrecord
    properties = [
        "price",
        "id",
    ]
    def get_extra_data(self, o):
        return {
        "carvin": o.carvin.vin,
        "sales_person" : o.sales_person.name,
        "customer" : o.customer.name,
        }
    # encoders = {
    #     "carvin": AutomobileVOEncoder,
    #     "sales_person" : SalespersonListEncoder,
    #     "customer" : CustomerListEncoder


    # }

#add a sales person
@require_http_methods(["GET", "POST"])
def api_list_AutomobileVO(request):
    if request.method == "GET":
        automobilevo = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobilevo": automobilevo},
            encoder = AutomobileVOEncoder,
            safe=False
            )






@require_http_methods(["GET", "POST"])
def api_list_salespersons(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder = SalespersonListEncoder,
            safe=False
            )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder= SalespersonListEncoder,
            safe = False)

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerListEncoder,
            safe=False
            )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder= CustomerListEncoder,
            safe = False)


@require_http_methods(["GET", "POST"])
def api_sales_records(request):
    if request.method == "GET":
        sales_records = Salesrecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder= SalesRecordEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        content = {
            **content,
            "sales_person": Salesperson.objects.get(pk=content["sales_person"]),
            "carvin": AutomobileVO.objects.get(pk=content["carvin"]),
            "customer": Customer.objects.get(pk=content["customer"]),





        }
        sales_records = Salesrecord.objects.create(**content)
        return JsonResponse(
            sales_records,
            encoder = SalesRecordEncoder,
            safe = False)


# class SalesRecordEncoder(ModelEncoder):
#     model = Salesrecord
#     properties = [
#         "vin",
#         "sales_person",
#         "customer",
#         "price",
#         "id",
#     ]
#     encoders = {
#         "vin": AutomobileVOEncoder,
#         "sales_person" : SalespersonListEncoder,
#         "customer" : CustomerListEncoder


#     }
# create a

        #look at notes for picture

    #     try:
    #         location_href = content["location"]
    #         location = LocationVO.objects.get(import_href=location_href)

    #         content["location"] = location

    #     except:
    #         return JsonReposne(
	# 						{"Error": error})

    #     hat = Hat.objects.create(**content)
    #     return JsonResponse(
    #             hat,
    #             encoder=HatDetailEncoder,
    #             safe=False
    #             )

#add a potential customer
# @require_http_methods(["GET", "POST"])
# def api_list_hats(request):
#     if request.method == "GET":
#         hats = Hat.objects.all()
#         return JsonResponse(
#             {"hats": hats},
#             encoder = HatListEncoder,
#             safe=False
#             )
#     else:
#         content = json.loads(request.body) #look at notes for picture

#         try:
#             location_href = content["location"]
#             location = LocationVO.objects.get(import_href=location_href)

#             content["location"] = location

#         except:
#             return JsonReposne(
# 							{"Error": error})

#         hat = Hat.objects.create(**content)
#         return JsonResponse(
#                 hat,
#                 encoder=HatDetailEncoder,
#                 safe=False
#                 )