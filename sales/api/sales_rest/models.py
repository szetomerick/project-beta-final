from django.db import models
from django.urls import reverse

#add a sales person
class Salesperson(models.Model):
  name = models.CharField(max_length= 200)
  number = models.CharField(max_length= 20)

  def __str__(self) -> str:
    return self.name + " " + str(self.number)
# added a customer to the sale
class Customer(models.Model):
  name = models.CharField(max_length= 200)
  address = models.CharField(max_length=200)
  number = models.CharField(max_length =20)

  def __str__(self) -> str:
    return self.name + " " + str(self.number)

# prbably going to import VIN or poller
class AutomobileVO(models.Model):
  vin = models.CharField(max_length=200)


#create a sales record with all the information needed, compiled from the foreignkeys

class Salesrecord(models.Model):
  sales_person = models.ForeignKey(Salesperson, related_name="salesrecords", on_delete=models.CASCADE)
  customer = models.ForeignKey(Customer, related_name="salesrecords", on_delete=models.CASCADE)
  carvin = models.ForeignKey(AutomobileVO, related_name="salesrecords", on_delete=models.CASCADE)
  price =  models.CharField(max_length=200)
