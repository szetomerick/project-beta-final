from django.contrib import admin

from.models import Salesperson, Customer, Salesrecord


@admin.register(Salesperson)
class Salesperson(admin.ModelAdmin):
    pass



@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass

@admin.register(Salesrecord)
class Salesrecord(admin.ModelAdmin):
    pass