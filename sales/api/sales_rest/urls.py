from django.contrib import admin
from django.urls import path
from .views import api_list_salespersons, api_list_customers, api_list_AutomobileVO, api_sales_records
urlpatterns = [
    path('salespersons/', api_list_salespersons, name = "api_list_salespersons"),
    path('customers/', api_list_customers, name = "api_list_customers"),
    path('automobilevo/', api_list_AutomobileVO, name = "api_list_automobilevo"),
    path('salesrecords/', api_sales_records, name = "api_sales_records"),


    # path("salespersons/<int:pk>/", api_details_salespersons, name="api_details_salespersons")
]
