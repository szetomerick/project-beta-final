import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './sales/SalesPersonForm';
import CustomerForm from './sales/CustomerForm';
import CreateSalesRecord from './sales/CreateSalesRecord';
import SalesRecordList from './sales/SalesRecordList';
import FilteredSalesRecordList from './sales/FilteredSalesList';
import ManufacturersAll from './inventory/ManufacturerList';
import CreateManufacturer from './inventory/CreateManufacturer';
import ModelList from './inventory/ModelList';
import ModelForm from './inventory/ModelForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/scp" element={<SalesPersonForm />} />
          <Route path="/newcustomer" element={<CustomerForm />} />
          <Route path="/newsalesrecord" element={<CreateSalesRecord/>} />
          <Route path="/salesrecordlist" element={<SalesRecordList />} />
          <Route path="/filteredsalesrecordlist" element={<FilteredSalesRecordList />} />
          <Route path="/manufacturersall" element={<ManufacturersAll />} />
          <Route path="/createmanufacturer" element={<CreateManufacturer />} />
          <Route path="/modellist" element={<ModelList/>} />
          <Route path="/modelform" element={<ModelForm/>} />



        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
