import React from 'react';

class ModelForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      picture_url: '',
      manufacturers: [],
      manufacturer: ''
    }
    this.handleNameChange = this.handleNameChange.bind(this)
    this.handlePictureChange = this.handlePictureChange.bind(this)
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  async componentDidMount() {
    const manufacturer_url = "http://localhost:8100/api/manufacturers/"
    const response = await fetch(manufacturer_url)
    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers })
    }
  }

  handleNameChange(event) {
    const value = event.target.value
    this.setState({ name: value })
  }

  handlePictureChange(event) {
    const value = event.target.value
    this.setState({ picture_url: value })
  }

  handleManufacturerChange(event) {
    const value = event.target.value
    this.setState({ manufacturer: value })
  }

  async handleSubmit(event) {
    event.preventDefault()
    const data = { ...this.state }
    delete data.manufacturers
    data.manufacturer_id=data.manufacturer
    delete data.manufacturer

    const url = "http://localhost:8100/api/models/"
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
      const newModel = await response.json()
    }
    const cleared = {
      name: '',
      picture_url: '',
      manufacturers: []
    }
    this.setState(cleared)
  }


  render() {
    return (
      <>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1 className>Model</h1>
              <form onSubmit={this.handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                  <input value={this.state.name} onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Name:</label>
                </div>


                <div className="form-floating mb-3">
                  <input value={this.state.picture_url} onChange={this.handlePictureChange} placeholder="Picture url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                  <label htmlFor="picture_url">Photo</label>
                </div>
                <div>


                  <select onChange={this.handleManufacturerChange} value={this.state.manufacturer_id} name="manufacturer_id" required id="manufacturer_id" className="form-select">
                    <option value="">Manufacturer</option>
                    {this.state.manufacturers?.map(manu => {
                      return (
                        <option key={manu.id} value={manu.id}>
                          {manu.name}
                        </option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default ModelForm