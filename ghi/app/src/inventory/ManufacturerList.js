import React from "react";

class ManufacturersAll extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturers: []
        }
    }
    async componentDidMount(){
    const salesrecordUrl = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(salesrecordUrl);

    if (response.ok) {
        const data = await response.json();
        this.setState({ manufacturers: data.manufacturers});
        }
    }
    render(){
        return (
            <>
            <h1> All Sales Below</h1>
            <table className = "table">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.manufacturers?.map(m => {
                        return (

                                <tr key={m.id}>
                                <td>{m.name}</td>
                                </tr>
                        )
                        })}
                </tbody>
            </table>
            </>
            )
        }
    }
export default ManufacturersAll