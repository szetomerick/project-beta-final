import React from "react";

class ModelList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      models: [],
    };
  }
  async componentDidMount() {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ models: data.models });
    }
  }
  render() {
    return (
      <>
        <h1>Models</h1>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {this.state.models?.map((m) => {
              return (
                <tr key={m.id}>
                  <td>{m.name}</td>
                  <td>{m.manufacturer.name}</td>
                  <td><img src={m.picture_url} /></td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
  }
}
export default ModelList;
