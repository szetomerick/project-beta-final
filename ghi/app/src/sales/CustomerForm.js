import React from 'react'

class CustomerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            number: "",
            address:"",
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleNumberChange = this.handleNumberChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
      }

    handleNumberChange(event) {
        const value = event.target.value;
        this.setState({number: value})
      }
    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({address: value})
      }


      async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}

        const customerurl = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(customerurl, fetchConfig)
        if (response.ok) {
                const newcustomer = await response.json()
                console.log(newcustomer)

                const cleared = {
                    name : " ",
                    number : " ",
                    address:" ",
                }

                this.setState(cleared)
        }
    }


    render() {
        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create A New Potential Customer</h1>
                  <form onSubmit={this.handleSubmit} id="create-customer-form">
                    <div className="form-floating mb-3">
                      <input value = {this.state.name} onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                      <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input value = {this.state.number} onChange={this.handleNumberChange} placeholder="Number" required type="text" name="number" id="number" className="form-control"/>
                      <label htmlFor="number">Number</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input value = {this.state.address} onChange={this.handleAddressChange} placeholder="Address" required type="text" name="address" id="address" className="form-control"/>
                      <label htmlFor="address">Address</label>
                    </div>
                    <button className="btn btn-primary">Create A Potential</button>
                  </form>
                </div>
              </div>
            </div>
          );
        }
      }

export default CustomerForm;