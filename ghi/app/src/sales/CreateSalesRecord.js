
import React from 'react'

class CreateSalesRecord extends React.Component {
    constructor(props) {
        super(props)
        this.state = {


            sales_person: [],
            customer : [],
            carvin : [],
            price : "",
        };
        this.handleSalespersonChange = this.handleSalespersonChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handleCarvinChange = this.handleCarvinChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

      }

    async componentDidMount() {
        fetch('http://localhost:8090/api/salespersons/')
            .then(response => response.json())
            .then(response =>this.setState({sales_person: response.salespersons}))
// CUSTOMER CHANGE FOR THIS ONE
        fetch('http://localhost:8090/api/customers/')
            .then(response => response.json())
            .then(response =>this.setState({customer: response.customers}))
//CARVIN CHANGE FOR THIS ONE
        fetch('http://localhost:8090/api/automobilevo/')
            .then(response => response.json())
            .then(response =>this.setState({carvin: response.automobilevo}))


    }
    handleSalespersonChange(event) {
        const value = event.target.value;
        this.setState({person: value})
    }
    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({cust: value})
    }
    handleCarvinChange(event) {
        const value = event.target.value;
        this.setState({vin: value})
    }
    handlePriceChange(event) {
        const value = event.target.value;
        this.setState({price: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.sales_person = data.person;
        delete data.carvin;
        data.carvin = data.vin;
        delete data.customer
        data.customer=data.cust;


        delete data.person
        delete data.cust;
        delete data.vin;


        const newSalesRecordUrl = 'http://localhost:8090/api/salesrecords/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(newSalesRecordUrl, fetchConfig);
        if (response.ok) {
            const newSalesRecord = await response.json();
            console.log(newSalesRecord);


            const cleared = {
                sales_person: [],
                customer : [],
                carvin : [],
                price : "",
            }
            this.setState(cleared)
        }
    }


    render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new Sales Form</h1>
                <form onSubmit={this.handleSubmit} id="create-sales-form">



                  <div className="mb-3">
                    <select onChange={this.handleSalespersonChange} required name="sales_person" id="sales_person" className="form-select" value={this.state.person}>
                      <option value="">Choose a Sales Person</option>
                      {this.state.sales_person.map(s => {
                        return (
                        <option key = {s.id} value={s.id}>
                            {s.name}
                        </option>
                        );
                    })}
                    </select>
                  </div>



                  <div className="mb-3">
                    <select onChange={this.handleCustomerChange} required name="customer" id="customer" className="form-select" value={this.state.cust}>
                      <option value="">Choose a Customer</option>
                      {this.state.customer.map(c => {
                        return (
                        <option key = {c.id} value={c.id}>
                            {c.name}
                        </option>
                        );
                    })}
                    </select>
                  </div>



                  <div className="mb-3">
                    <select onChange={this.handleCarvinChange} required name="carvin" id="carvin" className="form-select" value={this.state.vin}>
                      <option value="">Choose a Vin</option>
                      {this.state.carvin.map(b => {
                        return (
                        <option key = {b.id} value={b.id}>
                            {b.vin}
                        </option>
                        );
                    })}
                    </select>
                  </div>


                  <div className="form-floating mb-3">
                    <input onChange={this.handlePriceChange} placeholder="price" required type="text" name="price" id="price" className="form-control" value={this.state.price}/>
                    <label htmlFor="price">Price</label>
                  </div>

                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }

  export default CreateSalesRecord;