import React from "react";



class FilteredSalesRecordList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            salespersons: [],
            sales:[],
            sales_person:""
        }
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this)
      }
      handleSalesPersonChange(event) {
        const value = event.target.value
        this.setState({ sales_person: parseInt(value) })

      }



    async componentDidMount(){
    const salespersonUrl = 'http://localhost:8090/api/salespersons/'
    const salesrecordsURL = 'http://localhost:8090/api/salesrecords/';
    const salesrecordUrlresponse = await fetch(salesrecordsURL);
    const salespersonresponse = await fetch(salespersonUrl);

    if (salesrecordUrlresponse.ok) {
        const datasales = await salesrecordUrlresponse.json();
        this.setState({ sales_records: datasales.sales_records });
        }
    if (salespersonresponse.ok) {
        const data = await salespersonresponse.json();
        console.log("data::::::",data)
        this.setState({ salespersons: data.salespersons });
        }
    }



    render(){
        return (
            <>
            <h1> All Sales History Below</h1>
                <select onChange={this.handleSalesPersonChange} value={this.state.sales_person} name="salespersons" required id="salespersons" className="form-select">
                        <option className="text-center" value="">Choose Sales Person</option>
                        {this.state.salespersons?.map(s => {
                        return (
                            <option key={s.id} value={s.number}>
                            {s.name}
                            </option>
                        )
                        })}
                    </select>
            <table className = "table">
                <thead>
                    <tr>
                        <th>Sales Person Name</th>
                        <th>Customer</th>
                        <th>vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.sales_records?.filter(sales => sales.sales_person.number === this.state.sales_person).map(s=>{
                        {console.log("merick::::::::",sales_records)}
                        return (
                            <tr key={s.id}>
                            <td>{s.sales_person.name}</td>
                            <td>{s.customer.name}</td>
                            <td>{s.carvin}</td>
                            <td>{s.price}</td>
                            </tr>
                        )

                        })}
                </tbody>
            </table>
            </>
            )
        }
    }


export default FilteredSalesRecordList