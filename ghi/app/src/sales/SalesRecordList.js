import React from "react";



class SalesRecordList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales_records: []
        }
    }
    async componentDidMount(){
    const salesrecordUrl = 'http://localhost:8090/api/salesrecords/';
    const response = await fetch(salesrecordUrl);

    if (response.ok) {
        const data = await response.json();
        this.setState({ sales_records: data.sales_records });
        }
    }


    render(){
        return (
            <>
            <h1> All Sales Below</h1>
            <table className = "table">
                <thead>
                    <tr>
                        <th>Sales Person Name</th>
                        <th>Employee Number</th>
                        <th>Purchaser Name</th>
                        <th>Car Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.sales_records?.map((sales_records, i) => {
                        return (

                                <tr key={i}>
                                <td>{sales_records.sales_person}</td>
                                <td>{sales_records.sales_person}</td>
                                <td>{sales_records.customer}</td>
                                <td>{sales_records.carvin}</td>
                                <td>{sales_records.price}</td>
                                </tr>
                        )
                        })}
                </tbody>
            </table>
            </>
            )
        }
    }


export default SalesRecordList