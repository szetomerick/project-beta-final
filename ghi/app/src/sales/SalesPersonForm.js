import React from 'react'

class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            number: "",
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleNumberChange = this.handleNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
      }

    handleNumberChange(event) {
        const value = event.target.value;
        this.setState({number: value})
      }

      async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}

        const salespersonsurl = 'http://localhost:8090/api/salespersons/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(salespersonsurl, fetchConfig)
        if (response.ok) {
                const newsalesperson = await response.json()
                console.log(newsalesperson)

                const cleared = {
                    name : " ",
                    number : " "
                }

                this.setState(cleared)
        }
    }


    render() {
        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>Create A New  SalesPerson</h1>
                  <form onSubmit={this.handleSubmit} id="create-salesperson-form">
                    <div className="form-floating mb-3">
                      <input  onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                      <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input  onChange={this.handleNumberChange} placeholder="Number" required type="text" name="number" id="number" className="form-control"/>
                      <label htmlFor="number">Number</label>
                    </div>
                    <button className="btn btn-primary">Create A SalesPerson</button>
                  </form>
                </div>
              </div>
            </div>
          );
        }
      }

export default SalesPersonForm;