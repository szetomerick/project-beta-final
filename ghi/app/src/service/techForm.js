import React from 'react';

class TechnicianForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            employee_number: '',
        }


        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)

    }
    handleChange(event) {
        const value = event.target.value;
        this.setState({value})
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.employee_number = data.employeeNumber
        data.name = data.name
        delete data.employeeNumber
        console.log(data)

        const techURL = "http://localhost:8080/api/technician/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(techURL, fetchConfig);
        if (response.ok) {
            const apt = await response.json();
            console.log(apt)
            this.setState({
                name: '',
                employee_number: '',
            });
        }
        }

    


    render() {

        return (
            <div className="row">
              <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                  <h1>New Technician</h1>
                  <form onSubmit={this.handleSubmit} id="create-technician-form" >
                    <div className="form-floating mb-3">
                      <input onChange={this.handleChange} value={this.state.Name} placeholder="Name" required type="text" id="name" className="form-control" />
                      <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input onChange={this.handleChange} value={this.state.employeeNumber} placeholder="Employee number" required type="text" id="employeeNumber" className="form-control" />
                      <label htmlFor="employeeNumber">Employee number</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
        )
    }

}
export default TechnicianForm;
