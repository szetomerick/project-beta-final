import React from 'react';

class AutomobileVOForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            import_href: '',
            VIN: '',
        }


        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)

    }
    handleChange(event) {
        const value = event.target.value;
        this.setState({value})
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.employee_number = data.employeeNumber
        data.name = data.name
        delete data.employeeNumber
        console.log(data)

        const techURL = "http://localhost:8080/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(techURL, fetchConfig);
        if (response.ok) {
            const apt = await response.json();
            console.log(apt)
            this.setState({
                import_href: '',
                VIN: '',
            });
        }
        }