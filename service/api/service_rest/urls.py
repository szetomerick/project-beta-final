
from django.urls import path

from .views import (
    api_list_appointment,
    api_list_automobileVO,
    api_service_history,
    api_show_appointments,
    api_list_technician,
    api_delete_technician,
)




urlpatterns = [
    path("services/", api_list_appointment, name="api_list_services"),
    path("services/<int:VIN>/", api_show_appointments, name="api_show_appt"),
    path("automobiles/", api_list_automobileVO, name="api_list_automobileVO"),
    path("history/<str:VIN>/", api_service_history, name="api_service_history"),
    path("technician/", api_list_technician, name="api_list_technician"),
    path("technician/<str:pk>/", api_list_technician, name="api_list_technician"),
    path("technician/<str:pk>/", api_delete_technician, name="api_delete_technician"),
]